import React from 'react'
import TodoItem from './TodoItem'
import {useState} from 'react'



export default function TodoList(props) {
  const[Input,setInput] = useState("")
  
  const handleSubmit = (e) =>{
     e.preventDefault()
     props.addTodo(Input)
     setInput("")
  }
  return (
    <form onSubmit = {handleSubmit} className ="todo-form">
      <h1>My Todo List</h1>
        <input
        placeholder='Nhập vào đây...' 
        value ={Input}
        onChange ={(e) => 
          setInput(e.target.value)
        }
        className="todo-input"/>
        <button type = "submit" className="todo-button">Thêm</button>
      </form>
  )
}
